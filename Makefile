
clean:
	rm -f ccschemer		\
	 ccschemer.build.sh	\
	 ccschemer.install.sh	\
	 *.o			\
	 *.obj			\
	 *.so			\
	 *.import.scm		\
	 *.link
