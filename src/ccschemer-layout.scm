;; This file is part of ccschemer.
;; Copyright (C) 2014-2015  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(module ccschemer-layout
    *

(import (chicken base)
        scheme)

(import coops
        miscmacros)


;;
;; Parameters
;;

(define page-width (make-parameter 800))

(define current-layout (make-parameter #f))


;;
;; Rectangle
;;

(define-record rect
  x y width height)


;;
;; Layout
;;

(define-generic (layout-end this))

(define-generic (%line-break this))

(define (line-break)
  (%line-break (current-layout)))

(define-generic (%place-widget this))

(define (place-widget width height)
  (%place-widget (current-layout) width height))

(define-class <layout> ()
  ((width: initform: (page-width))
   (top: initform: 0)
   (pt-x: initform: 0)
   (pt-y: initform: 0)))

(define (layout-append! layout #!key (skip 0))
  (let ((current (current-layout)))
    (layout-end current)
    (let ((x (slot-value current pt-x:))
          (y (+ skip (slot-value current pt-y:))))
      (set! (slot-value layout top:) y)
      (set! (slot-value layout pt-x:) x)
      (set! (slot-value layout pt-y:) y))
    (current-layout layout)))


;;
;; Rows-layout
;;

(define-class <rows-layout> (<layout>)
  ((%current-row-height: initform: 0)))

(define-method (layout-end (this <rows-layout>))
  (%line-break this))

(define-method (%line-break (this <rows-layout>))
  (set! (slot-value this pt-x:) 0)
  (set! (slot-value this pt-y:)
        (+ (slot-value this pt-y:)
           (slot-value this %current-row-height:)))
  (set! (slot-value this %current-row-height:) 0))

(define-method (%place-widget (this <rows-layout>) width height)
  (when (> (+ (slot-value this pt-x:) width) (slot-value this width:))
    (%line-break this))
  (begin0
   (make-rect (slot-value this pt-x:) (slot-value this pt-y:) width height)
   (set! (slot-value this pt-x:) (+ (slot-value this pt-x:) width))
   (set! (slot-value this %current-row-height:)
         (max (slot-value this %current-row-height:) height))))


;;
;; Columns-layout
;;

(define-class <columns-layout> (<layout>)
  ((%current-column-width: initform: 0)
   (%max-y: initform: 0)))

(define-method (layout-end (this <columns-layout>))
  (set! (slot-value this pt-x:) 0)
  (set! (slot-value this pt-y:) (slot-value this %max-y:)))

(define-method (%line-break (this <columns-layout>))
  (set! (slot-value this pt-x:)
        (+ (slot-value this pt-x:)
           (slot-value this %current-column-width:)))
  (set! (slot-value this pt-y:) (slot-value this top:))
  (set! (slot-value this %current-column-width:) 0))

(define-method (%place-widget (this <columns-layout>) width height)
  (begin0
   (make-rect (slot-value this pt-x:) (slot-value this pt-y:) width height)
   (set! (slot-value this pt-y:) (+ (slot-value this pt-y:) height))
   (set! (slot-value this %current-column-width:)
         (max (slot-value this %current-column-width:) width))
   (set! (slot-value this %max-y:)
         (max (slot-value this %max-y:) (slot-value this pt-y:)))))


;;
;; Initialization
;;

(current-layout (make <rows-layout>))

)
