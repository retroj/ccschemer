;; This file is part of ccschemer.
;; Copyright (C) 2014-2015  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(include-relative "ccschemer-language")
(include-relative "ccschemer-color")
(include-relative "ccschemer-event")
(include-relative "ccschemer-layout")

(module ccschemer
    *

(import (chicken base)
        (chicken keyword)
        (chicken module)
        (chicken port)
        (chicken random)
        (chicken sort)
        scheme)

(import (srfi 13)
        coops-utils
        filepath
        memoize
        regex
        ssax
        sxml-transforms
        sxpath)

(reexport (chicken format)
          (chicken string)
          (srfi 1)
          coops
          fmt
          list-utils
          matchable
          miscmacros)

(reexport ccschemer-language
          ccschemer-color
          ccschemer-event
          ccschemer-layout)


(define make/copy make-copy)
(define randomize set-pseudo-random-seed!)

;;
;; Parameters
;;

(define current-document (make-parameter #f))

(define angle-scale (make-parameter 1))

(define angle-offset (make-parameter 0))

(define button-color (make-parameter (rgb 0.1 0.1 0.1)))

(define button-height (make-parameter 100))

(define button-width (make-parameter 100))


;;
;; Basic Colors
;;

(define color:black (rgb 0 0 0))

(define color:white (rgb 1 1 1))


;;
;; Util
;;

(define (program-abort msg #!key (error-code 1))
  (fmt (current-error-port) "Error: " msg nl nl)
  (exit error-code))

(define (log-status . args)
  (fmt (current-error-port) (apply-cat args) nl))

(define (log-warn . args)
  (fmt (current-error-port) "Warning: " (apply-cat args) nl))

(define tau (* 4.0 (asin 1.0)))
(define tau/2 (* 2.0 (asin 1.0)))

(define (angle->radians angle)
  (* tau (/ (+ (angle-offset) angle)
            (angle-scale))))

(define (radians->angle r)
  (- (* (angle-scale)
        (/ r tau))
     (angle-offset)))

(define (normalize-radians angle)
  (let* ((d (truncate (/ angle tau)))
         (e (- angle (* d tau))))
    (if (>= e 0)
        e
        (+ tau e))))

(define (translate-speed x)
  (let ((x (/ x (angle-scale))))
    (if (= 0 x)
        0
        (/ 1.0 x))))

(define (bool->number x)
  (if x
      1
      0))

(define (number->bool x)
  (if (zero? x)
      #f
      #t))

;; interpolate n steps between angles a and b
;; *mostly for internal use
;;
(define (interpolate-angles a b n)
  (let* ((b (if (>= b a) b (+ (abs (angle-scale)) b)))
         (d (- b a))
         (step (/ d (+ 1 n))))
    (list-tabulate n (lambda (i) (+ a (* (+ 1 i) step))))))

;; interpolate-angles-list takes a list of angles and boolean false (#f)
;; values, and returns a list in which the boolean falses are replaced by
;; interpolated angles between known values given in the source list.
;;
(define (interpolate-angles-list lst)
  (let* ((first-known #f)
         (last-known #f)
         (leading-unknowns 0)
         (prepend-elements #f))

    (define (loop unknowns lst)
      (match lst
        (() ;; reached end of list
         (cond
          ((not first-known)
           (list-tabulate unknowns
                          (lambda (i)
                            (* (abs (angle-scale))
                               (/ i unknowns)))))
          ((or (> unknowns 0) (> leading-unknowns 0))
           (let-values (((append-els prepend-els)
                         (split-at (interpolate-angles
                                    last-known first-known
                                    (+ leading-unknowns unknowns))
                                   unknowns)))
             (set! prepend-elements prepend-els)
             append-els))
          (else '())))
        ((#f . xs) ;; an unknown angle
         (loop (+ 1 unknowns) (rest lst)))
        ((x . xs) ;; a known angle
         (unless first-known
           (set! first-known x)
           (set! leading-unknowns unknowns))
         (let ((ppx last-known))
           (set! last-known x)
           (append!
            (if ppx
                (interpolate-angles ppx x unknowns)
                '())
            (cons x (loop 0 (rest lst))))))))

    (let ((zult (loop 0 lst)))
      (if (> leading-unknowns 0)
          (append! prepend-elements zult)
          zult))))

;; intersperse* takes a list and a function of two arguments and calls the
;; function on each successive pair of elements in the list, interspersing
;; the results of these calls between the original elements in the
;; resulting list.  If the optional 'wrap?' is given, the function is
;; also called on the last and first elements of the list, and the result
;; of that call appended to the result.
;;
(define (intersperse* lst fn #!optional (wrap? #f))
  (if (null? lst)
      lst
      (reverse!
       (pair-fold
        (lambda (pair acc)
          (let* ((at-end? (null? (cdr pair)))
                 (n1 (car pair))
                 (n2 (if at-end?
                         (car lst)
                         (cadr pair))))
            (if (or (not at-end?) wrap?)
                (cons (fn n1 n2) (cons n1 acc))
                (cons n1 acc))))
        (list)
        lst))))

(define (interpolate-value-sinusoidal a b a-time b-time current-time)
  (let* ((ratio (/ (- current-time a-time) (- b-time a-time)))
         (r (+ (* 0.5 (- (cos (* 0.25 tau ratio)))) 0.5)))
    (+ a (* r (- b a)))))

(define (chromacove-version>= n)
  (let ((v (with-input-from-string (chromacove-version) read)))
    (>= v n)))


;;
;; Datatypes
;;

(define-generic (->ccproj-sxml this))


;; node
;;

(define-class <node> ()
  ((angle: initform: #f)
   (r1: initform: 0.0) ;; inner radius for color spot
   (r2: initform: 0.0) ;; outer radius for color spot
   (bias: initform: 0.5) ;; not sure what this is
   (color: initform: color:black)
   (color2: initform: color:black) ;; outer radius color
   (alpha: initform: 1.0)))

(define (node? x) (instance-of? x <node>))

(define (node color . properties)
  (apply make <node> color: color properties))

(define (node-from-sxml node-sxml)
  (let* ((props (cdr node-sxml))
         (angle (radians->angle (string->number (car (alist-ref 'angle props)))))
         (bias (string->number (car (alist-ref 'bias props))))
         (color (car (alist-ref 'color props)))
         (number-regexp '(+ printing))
         (color-parsed (string-match `(: ($ ,number-regexp) #\, (* whitespace)
                                         ($ ,number-regexp) #\, (* whitespace)
                                         ($ ,number-regexp) #\, (* whitespace)
                                         ($ ,number-regexp))
                                     color))
         (red (string->number (list-ref color-parsed 1)))
         (green (string->number (list-ref color-parsed 2)))
         (blue (string->number (list-ref color-parsed 3)))
         (alpha (string->number (list-ref color-parsed 4))))
    (node (rgb red green blue)
          angle: angle
          bias: bias
          alpha: alpha)))

(define-method (->ccproj-sxml (this <node>) #!optional angle)
  (define (format-color c)
    (string-join
     (map ->string
          (append
           (color->RGB709 c)
           (list
            (slot-value this alpha:))))
     ", "))
  (define (format-angle a)
    (normalize-radians
     (angle->radians a)))
  (let* ((color (format-color (slot-value this color:)))
         (color2 (format-color (slot-value this color2:)))
         (center (or angle (slot-value this angle:)))
         (r1 (slot-value this r1:))
         (r2 (slot-value this r2:)))
    `(,@(if (zero? r2)
            '()
            `((node
               (angle ,(format-angle (- center r2)))
               (bias ,(slot-value this bias:))
               (color ,color2))))
      ,@(if (zero? r1)
            `((node
               (angle ,(format-angle center))
               (bias ,(slot-value this bias:))
               (color ,color)))
            `((node
               (angle ,(format-angle (- center r1)))
               (bias ,(slot-value this bias:))
               (color ,color))
              (node
               (angle ,(format-angle (+ center r1)))
               (bias ,(slot-value this bias:))
               (color ,color))))
      ,@(if (zero? r2)
            '()
            `((node
               (angle ,(format-angle (+ center r2)))
               (bias ,(slot-value this bias:))
               (color ,color2)))))))


;; layer (base class)
;;

(define layer-types '(repeat stop ping-pong))

(define (layer-type->number type)
  (list-index
   (lambda (x) (eq? type x))
   layer-types))

(define (number->layer-type x)
  (list-ref layer-types x))

(define blend-modes
  '(normal add subtract multiply tint
    linear-burn saturate overlay))

(define (blend-mode->number mode)
  (list-index
   (lambda (x) (eq? mode x))
   blend-modes))

(define (number->blend-mode x)
  (list-ref blend-modes x))

(define tween-types
  '(linear
    quadratic-ease-in
    quadratic-ease-out
    quadratic-ease-in-out
    cubic-ease-in
    cubic-ease-out
    cubic-ease-in-out
    quartic-ease-in
    quartic-ease-out
    quartic-ease-in-out
    sine-ease-in
    sine-ease-out
    sine-ease-in-out
    exponential-ease-in
    exponential-ease-out
    exponential-ease-in-out
    circular-ease-in
    circular-ease-out
    circular-ease-in-out))

(define (tween->number tween)
  (list-index
   (lambda (x) (eq? tween x))
   tween-types))

(define (number->tween x)
  (list-ref tween-types x))

(define-class <layer> ()
  ((name: initform: "Untitled Layer")
   (startangle: initform: (- 0.0 (angle-offset)))
   (stopangle: initform: (- (angle-scale) (angle-offset)))
   (speed: initform: 0.0)
   (type: initform: 'repeat)
   (tween: initform: 'linear)
   (blend: initform: 'normal)
   (visible: initform: #t)
   (antialiased: initform: #t))) ;; no idea what this is

(define (layer? x) (instance-of? x <layer>))

(define (layer-common-properties-from-sxml layer-sxml)
  (let* ((props (cdr layer-sxml))
         (get-prop (lambda (key) (string->number (car (alist-ref key props)))))
         (name (alist-ref 'name props)))
    (list name:        (if (null? name) "" (car name))
          startangle:  (radians->angle     (get-prop 'startAngle))
          stopangle:   (radians->angle     (get-prop 'stopAngle))
          speed:       (translate-speed    (get-prop 'speed))
          type:        (number->layer-type (get-prop 'type))
          tween:       (number->tween      (get-prop 'tween))
          blend:       (number->blend-mode (get-prop 'blend))
          visible:     (number->bool       (get-prop 'isVisible))
          antialiased: (number->bool       (get-prop (if (chromacove-version>= 1.26)
                                                         'antiAliased
                                                         'antialiased))))))

(define (layer-common-ccproj-sxml ob)
  `((name ,(slot-value ob name:))
    (startAngle ,(angle->radians (slot-value ob startangle:)))
    (stopAngle ,(angle->radians (slot-value ob stopangle:)))
    (speed ,(translate-speed (slot-value ob speed:)))
    (type ,(layer-type->number (slot-value ob type:)))
    (tween ,(tween->number (slot-value ob tween:)))
    (blend ,(blend-mode->number (slot-value ob blend:)))
    (isVisible ,(bool->number (slot-value ob visible:)))
    (,(if (chromacove-version>= 1.26)
          'antiAliased
          'antialiased)
     ,(bool->number (slot-value ob antialiased:)))))


;; gradient
;;

(define-class <gradient> (<layer>)
  ((nodes: initform: (list))))

(define-method (initialize-instance (this <gradient>))
  (call-next-method)
  (set! (slot-value this nodes:)
        (map (lambda (x)
               (cond
                ((node? x) x)
                ((color? x) (node x))
                (else
                 (ccschemer-error '<gradient>::initialize-instance
                                  "invalid node" x))))
             (slot-value this nodes:))))

(define (gradient? x) (instance-of? x <gradient>))

(define (gradient nodes . properties)
  (apply make <gradient> nodes: nodes properties))

(define (gradient-from-sxml gradient-sxml)
  (let* ((gradient-sxml-doc (list 'doc gradient-sxml))
         (nodes-sxml ((sxpath '(// gradient gradient nodes node)) gradient-sxml-doc)))
    (apply gradient
           (map node-from-sxml nodes-sxml)
           (layer-common-properties-from-sxml gradient-sxml))))

(define-method (->ccproj-sxml (this <gradient>))
  `(gradient
    ,@(layer-common-ccproj-sxml this)
    (gradient
     (nodes
      ,@(let* ((nodes (slot-value this nodes:))
               (angles (map (cut slot-value <> angle:) nodes)))
          ;;XXX: slight breakage of abstraction to sort the nodes _after_
          ;;     converting them to sxml.
          (sort
           ;; each <node> produces a list of nodes (to support color
           ;; spots), so we must flatten the list to sort it.
           (apply append
                  (map (lambda (x) (->ccproj-sxml (cdr x) (car x)))
                       (zip-alist (interpolate-angles-list angles)
                                  nodes)))
           (lambda (a b)
             (let ((aa (cadr (assq 'angle (cdr a))))
                   (ba (cadr (assq 'angle (cdr b)))))
               (< aa ba)))))))))


;; solid-color
;;

(define-class <solid-color> (<gradient>)
  ((color: initform: color:black))) ;;XXX: use a setter to update nodes list

(define-method (initialize-instance (this <solid-color>))
  (set! (slot-value this nodes:) (list (make <node> color: (slot-value this color:))))
  (call-next-method))

(define (solid-color? x) (instance-of? x <solid-color>))

(define (solid-color color . properties)
  (apply make <solid-color> color: color properties))


;; color-spot
;;

(define-class <color-spot> (<gradient>)
  ((color: initform: color:white)
   (color2: initform: color:black)
   (angle: initform: 0.0)
   (r1: initform: 0.0)
   (r2: initform: 0.5)))

(define-method (initialize-instance (this <color-spot>))
  (call-next-method)
  (let ((color (slot-value this color:))
        (color2 (slot-value this color2:))
        (angle (slot-value this angle:))
        (r1 (slot-value this r1:))
        (r2 (slot-value this r2:)))
    (set! (slot-value this nodes:)
          (list (make <node> color: color color2: color2
                      angle: angle r1: r1 r2: r2)))))

(define (color-spot? x) (instance-of? x <color-spot>))

(define (color-spot color angle . properties)
  (apply make <color-spot> color: color angle: angle properties))


;; group
;;

(define-class <group> (<layer>)
  ((name: initform: "")
   (layers: initform: (list))))

(define-method (initialize-instance (this <group>))
  (call-next-method)
  (let ((layers (slot-value this layers:)))
    (unless (list? layers)
      (set! (slot-value this layers:) (list layers)))))

(define (group? x) (instance-of? x <group>))

(define (group layers . properties)
  (apply make <group> layers: layers properties))

(define (group-from-sxml group-sxml)
  (let* ((group-sxml-doc (list 'doc group-sxml))
         (layers-sxml ((sxpath '(// group layers *)) group-sxml-doc)))
    (apply group
           (map (lambda (layer-sxml)
                  (case (car layer-sxml)
                    ((gradient) (gradient-from-sxml layer-sxml))
                    ((group) (group-from-sxml layer-sxml))))
                layers-sxml)
           (layer-common-properties-from-sxml group-sxml))))

(define-method (->ccproj-sxml (this <group>))
  `(group
    ,@(layer-common-ccproj-sxml this)
    (layers
     ,@(map ->ccproj-sxml (slot-value this layers:)))))


;; chromacove-project
;;

(define-class <chromacove-project> ()
  ((src-path: initform: #f)
   (version: initform: "1.12 beta")
   (cues: initform: (list))
   (buttons: initform: (list))
   (cue-id-counter: initform: (make-counter 100))
   (selection: initform: #f)))

(define (chromacove-project? x) (instance-of? x <chromacove-project>))

(define (chromacove-project . properties)
  (apply make <chromacove-project> properties))

(define (chromacove-project-from-sxml ccproj-sxml)
  (parameterize ((current-document (make <chromacove-project>)))
    (let ((ccproj-version ((if-car-sxpath '(// ChromaCoveProject version)) ccproj-sxml)))
      (if ccproj-version
          (set! (slot-value (current-document) version:)
                (cadr ccproj-version))
          (log-warn "version not found in document, using default of "
                    (slot-value (current-document) version:)))
      (for-each
       cue-from-sxml
       ((sxpath '(// ChromaCoveProject cues cue)) ccproj-sxml))
      (for-each
       button-from-sxml
       ((sxpath '(// ChromaCoveProject liveSurface buttons button)) ccproj-sxml)))
  (current-document)))

(define (chromacove-version #!optional version-str)
  (if version-str
      (set! (slot-value (current-document) version:) version-str)
      (slot-value (current-document) version:)))

(define-method (->ccproj-sxml (this <chromacove-project>))
  `(ChromaCoveProject
    (version ,(slot-value this version:))
    (cues
     ,@(map ->ccproj-sxml (reverse (slot-value this cues:))))
    (liveSurface
     (buttons
      ,@(map ->ccproj-sxml (reverse (slot-value this buttons:)))))))

(define get-cue
  (case-lambda
   ((project name default)
    (let ((cues (slot-value project cues:))
          (name (if (symbol? name) name (string->symbol name))))
      (or (find (lambda (cue) (eq? name (slot-value cue name:))) cues)
          default)))
   ((project name)
    (or (get-cue project name #f)
        (ccschemer-error 'get-cue "no such cue" name)))))

(define (get-cue-by-id project id)
  (find (lambda (cue) (= id (slot-value cue id:)))
        (slot-value project cues:)))

(define (add-cue! project cue)
  (let ((cues (slot-value project cues:))
        (name (slot-value cue name:)))
    (if (get-cue project name #f)
        (ccschemer-error 'add-cue! "duplicate cue name" name)
        (set! (slot-value project cues:)
              (cons cue cues)))
    cue))

(define (add-button! project button)
  (and-let* ((id (slot-value button id:))
             (other (and (> id 0)
                         (find (lambda (b) (= id (slot-value b id:)))
                               (slot-value project buttons:)))))
    (log-warn 'add-button! "duplicate button id"
              id
              (slot-value button name:)
              (slot-value other name:)))
  (set! (slot-value project buttons:)
        (cons button (slot-value project buttons:))))

(define (import-buttons from-project #!optional (cue-edit identity))
  (for-each
   (lambda (b)
     (let ((cuerefs
            (map
             (lambda (ref)
               (let* ((name (slot-value ref cue:))
                      (importcue (cue-edit (get-cue from-project name))))
                 (unless (get-cue (current-document) name #f)
                   (cue name
                        id: (get-cue-id)
                        brightness: (slot-value importcue brightness:)
                        layers: (slot-value importcue layers:)
                        dmx: (slot-value importcue dmx:)))
                 (make/copy ref)))
             (slot-value b cues:))))
       (add-button!
        (current-document)
        (make/copy b cues: cuerefs
                   rect: (let ((r (slot-value b rect:)))
                           (place-widget (rect-width r) (rect-height r)))))))
   (slot-value from-project buttons:)))

(define (chromacove-project-generate-output-name project)
  (and-let* ((path (slot-value project src-path:)))
    (filepath:add-extension
     (filepath:drop-extension path)
     "ccproj")))

(define (chromacove-project-selection project #!key (include-button-cues #f) (reset #f))
  (let ((selection (slot-value project selection:)))
    (when reset
      (set! (slot-value project selection:) #f))
    (if selection
        (receive (cues buttons)
            (partition cue? selection)
          (list
           (delete-duplicates
            (if include-button-cues
                (append cues
                        (append-map
                         (lambda (button)
                           (map
                            (lambda (cue) (get-cue project cue))
                            (map
                             (lambda (ref) (slot-value ref cue:))
                             (slot-value button cues:))))
                         buttons))
                cues))
           (delete-duplicates buttons)))
        (list (slot-value project cues:) (slot-value project buttons:)))))


;; cue
;;

(define (get-cue-id)
  ((slot-value (current-document) cue-id-counter:)))

(define cue-name-prefix (make-parameter #f))

(define cue-generate-name-prefix (make-parameter #f))

(define (cue-generate-name given)
  (let ((prefixstr (->string (or (cue-name-prefix) "")))
        (genprefix (->string (or (cue-generate-name-prefix) ""))))
    (if given
        (string->symbol (string-append prefixstr (->string given)))
        (gensym (string-append prefixstr genprefix)))))

(define-class <cue> ()
  ((id: initform: (get-cue-id))
   (name:)
   (brightness: initform: 1.0)
   (layers: initform: (make <group>))
   (dmx: initform: #f)))

(define-method (initialize-instance (this <cue>))
  (call-next-method)
  (set! (slot-value this name:)
        (cue-generate-name (slot-value this name:))))

(define (cue? x) (instance-of? x <cue>))

(define (cue name . layers+properties)
  (let ((cue (apply make <cue> name: name
                    (if (or (null? layers+properties)
                            (keyword? (first/default layers+properties #f)))
                        layers+properties
                        (cons layers: layers+properties)))))
    (add-cue! (current-document) cue)
    cue))


(define (cue-from-sxml cue-sxml)
  (let* ((props (rest cue-sxml))
         ;;XXX: increment document counter
         (id (string->number (cadr ((car-sxpath '(cue @ id))
                                    (list 'doc cue-sxml)))))
         (name (string->symbol (car (alist-ref 'name props))))
         (brightness (string->number (car (alist-ref 'brightness props))))
         (group (assq 'group props))
         (dmx (alist-ref 'dmx props))
         (dmx-enabled (number->bool (string->number (car (alist-ref 'enabled dmx)))))
         (dmx-counter -1))
    (apply cue name (group-from-sxml group)
           id: id
           brightness: brightness
           (if dmx-enabled
               (list dmx:
                     (fold
                      (lambda (x result)
                        (cond
                         ((eq? 'channel (car x))
                          (set! dmx-counter (+ 1 dmx-counter))
                          (let ((dmxval (string->number (cadr x))))
                            (if (zero? dmxval)
                                result
                                (cons (cons dmx-counter dmxval) result))))
                         (else result)))
                      '()
                      dmx))
               '()))))

(define-method (->ccproj-sxml (this <cue>))
  `(cue
    (@ (id ,(->string (slot-value this id:))))
    (name ,(->string (slot-value this name:)))
    (brightness ,(slot-value this brightness:))
    ,(->ccproj-sxml
      (let ((layers (slot-value this layers:)))
        (cond
         ((group? layers) layers)
         ((and (list? layers)
               (= 1 (length layers))
               (group? (first layers)))
          (first layers))
         (else
          (make <group> layers: layers)))))
    (dmx
     ,@(let ((dmx (or (slot-value this dmx:) '())))
         (list-tabulate 512 (lambda (i)
                              (if* (assoc i dmx)
                                   `(channel ,(cdr it))
                                   `(channel 0)))))
     (enabled ,(bool->number (slot-value this dmx:))))))

(define (cues-add properties . cues)
  (apply cue #f
         (map
          (lambda (cue)
            (group (slot-value cue layers:)
                   blend: 'add))
          cues)
         properties))


;; cueref
;;

(define-class <cueref> ()
  ((cue:)
   (fade: initform: 0)
   (hold: initform: 0)))

(define-method (initialize-instance (this <cueref>))
  (call-next-method)
  (let ((o (slot-value this cue:)))
    ;; convert the reference to a symbol cue name
    (cond
     ((string? o)
      (set! (slot-value this cue:)
            (string->symbol o)))
     ((cue? o)
      (set! (slot-value this cue:)
            (slot-value o name:)))
     ((layer? o)
      (set! (slot-value this cue:)
            (slot-value (cue #f o) name:))))))

(define (cueref? x) (instance-of? x <cueref>))

(define (cueref cue . properties)
  (apply make <cueref> cue: cue properties))

(define-method (->ccproj-sxml (this <cueref>))
  (let ((cue (get-cue (current-document) (slot-value this cue:))))
    `(cue
      (cue ,(slot-value cue id:))
      (fadeTime ,(slot-value this fade:))
      (holdTime ,(slot-value this hold:)))))

(define (envelopes-intersect thisbright decay* nextbright next-cue-attack mix-time)
  (let* ((a (- (/ thisbright decay*))) ;; current cue decay slope
         (c thisbright) ;; current cue decay y intercept
         (b (/ nextbright next-cue-attack)) ;; next cue attack slope
         (d (- nextbright (* b mix-time))) ;; next cue attack y intercept
         (x (/ (- d c) (- a b)))
         (y (+ (* a x) c)))
    (values x y)))

(define (make-cue-sequence-simple-mixing timestamp attack* sustain* decay*
                                         next-cue-time* next-cue-attack
                                         cue result)
  ;; event overlaps next event.  compute (simplified) mixing.  this is a
  ;; simplified mixing method whereby we allow the current decay to mix
  ;; with the next attack, but sustains will remain pure, so only one cue
  ;; will be full on at a time, and only one mix-cue must be generated.
  ;; this requires some adjustment of the envelopes.
  (let* ((next-cueref (car result))
         (thisbright (slot-value cue brightness:))
         (nextbright (slot-value (get-cue (current-document) (slot-value next-cueref cue:)) brightness:))
         (post-time (exact->inexact (- next-cue-time* timestamp)))
         (mix-time (- post-time sustain*)))
    (cond
     ((>= sustain* post-time) ;; sustain extends into next sustain
      ;; truncate sustain, no mix node, next cue's fade set to 0.0
      (set! (slot-value next-cueref fade:) 0.0)
      (cons (cueref cue fade: attack* hold: post-time) result))
     ((zero? decay*)
      ;; sustain overlaps next-cue-attack, but there is no decay
      (let* ((b (/ nextbright next-cue-attack))
             (d (- nextbright (* b mix-time))) ;; y intercept of next-cue-attack
             (c (cues-add `(brightness: ,d)
                          (get-cue (current-document) (slot-value next-cueref cue:)))))
        (set! (slot-value next-cueref fade:) mix-time)
        (cons* (cueref cue fade: attack* hold: sustain*)
               (cueref (slot-value c name:) fade: 0.0 hold: 0.0)
               result)))
     ((zero? next-cue-attack)
      ;; decay overlaps next-timestamp
      (let* ((a (- (/ thisbright decay*)))
             (y (+ (* a mix-time) thisbright))
             (c (cues-add `(brightness: ,y) cue)))
        (cons* (cueref cue fade: attack* hold: sustain*)
               (cueref (slot-value c name:) fade: mix-time hold: 0.0)
               result)))
     (else
      ;; sustain or decay extends into next attack - compute mix-cue
      (let-values (((x y) (envelopes-intersect
                           thisbright decay*
                           nextbright next-cue-attack
                           mix-time)))
        (let* ((next-cue-fade (- mix-time x))
               (c (cues-add `(brightness: ,y)
                            cue (get-cue (current-document) (slot-value next-cueref cue:)))))
          ;;XXX: <group> does not have a brightness slot, so the best we
          ;;     can do here is a 50/50 mixture with an overall brightness
          ;;     on the whole mix.
          (set! (slot-value next-cueref fade:) next-cue-fade)
          (cons* (cueref cue fade: attack* hold: sustain*)
                 (cueref (slot-value c name:) fade: x hold: 0.0)
                 result)))))))

(define (make-cue-sequence event-list #!key (mix-mode 'simple)
                           (start-time #f)
                           (black-cue 'black))
  (define construct-cue cue)
  (define event-fold-1
    (let ((next-cue-time #f)
          (next-cue-attack 0.0))
      (bind-lambda*
       ((timestamp (attack sustain decay) . keywords) result)
       (let* ((attack* (or attack 0.0))
              (sustain* (or sustain 0.0))
              (decay* (or decay 0.0))
              (next-cue-time* (or next-cue-time (+ timestamp sustain* decay*)))
              (post-time (exact->inexact (- next-cue-time* timestamp)))
              (cue (or (alist-ref cue: keywords)
                       (ccschemer-error 'make-cue-sequence "event contains no cue: property")))
              (cue (cond
                    ((cue? cue) cue)
                    ((layer? cue) (construct-cue #f cue))
                    ((or (symbol? cue) (string? cue))
                     (get-cue (current-document) cue))
                    (else
                     (ccschemer-error 'make-cue-sequence "not a cue, cue name, or cue spec" cue)))))
         (begin0
          (cond
           ((and (eq? #f sustain) (eq? #f decay))
            ;; special case - no fade to black
            (cons (cueref cue fade: attack* hold: post-time) result))
           ((and next-cue-time
                 (> (+ sustain* decay*) (- post-time next-cue-attack)))
            ;; events overlap, compute mix-cue(s).
            (case mix-mode
              ((simple)
               (make-cue-sequence-simple-mixing timestamp attack* sustain* decay*
                                                next-cue-time* next-cue-attack
                                                cue result))
              (else
               (ccschemer-error 'make-cue-sequence "invalid mix-mode" mix-mode))))
           (else ;; events do not overlap
            (cons* (cueref cue fade: attack* hold: sustain*)
                   (cueref black-cue
                           fade: decay*
                           hold: (- next-cue-time* timestamp next-cue-attack sustain* decay*))
                   result)))
          (set! next-cue-time timestamp)
          (set! next-cue-attack attack*))))))
  (fold-right event-fold-1 '()
              (if start-time
                  (cons (make-event `(,start-time #f cue: ,black-cue))
                        event-list)
                  event-list)))


;; button
;;

(define-class <button> ()
  ((cues: initform: (list))
   (name:)
   (rect: initform: (place-widget (button-width) (button-height)))
   (brightness: initform: 1.0)
   (repeat: initform: #f)
   (id: initform: 0)
   (color: initform: (button-color))))

(define-method (initialize-instance (this <button>))
  (call-next-method)
  (set! (slot-value this cues:)
        (map (lambda (x)
               (cond
                ((cueref? x) x)
                ((cue? x) (cueref x))
                ((layer? x) (cueref (cue #f x)))))
             (ensure-list (slot-value this cues:)))))

(define (button? x) (instance-of? x <button>))

(define (button name . properties)
  (let ((button (apply make <button> name: name properties)))
    (add-button! (current-document) button)
    button))

(define (button-from-sxml button-sxml)
  (let* ((props (cdr button-sxml))
         (cuerefs-sxml (alist-ref 'cues props))
         (name (first/default (alist-ref 'name props) ""))
         (x (string->number (car (alist-ref 'x props))))
         (y (string->number (car (alist-ref 'y props))))
         (width (string->number (car (alist-ref 'width props))))
         (height (string->number (car (alist-ref 'height props))))
         (brightness (string->number (car (alist-ref 'brightness props))))
         (repeat (number->bool (string->number (car (alist-ref 'repeat props)))))
         (id (string->number (car (alist-ref 'id props))))
         (color (car (alist-ref 'color props)))
         (number-regexp '(+ printing))
         (color-parsed (string-match `(: ($ ,number-regexp) #\, (* whitespace)
                                         ($ ,number-regexp) #\, (* whitespace)
                                         ($ ,number-regexp))
                                     color))
         (red (string->number (list-ref color-parsed 1)))
         (green (string->number (list-ref color-parsed 2)))
         (blue (string->number (list-ref color-parsed 3))))
    (button name
            rect: (make-rect x y width height)
            brightness: brightness
            repeat: repeat
            id: id
            color: (rgb red green blue)
            cues: (map (lambda (cueref-sxml)
                         (let* ((props (cdr cueref-sxml))
                                (cue-id (string->number (car (alist-ref 'cue props))))
                                (cue (get-cue-by-id (current-document) cue-id))
                                (fade (string->number (car (alist-ref 'fadeTime props))))
                                (hold (string->number (car (alist-ref 'holdTime props)))))
                           (cueref cue fade: fade hold: hold)))
                       cuerefs-sxml))))

(define-method (->ccproj-sxml (this <button>))
  (let* ((rect (slot-value this rect:))
         (x (rect-x rect))
         (y (rect-y rect))
         (width (rect-width rect))
         (height (rect-height rect)))
    (log-status (slot-value this id:) "\t" (slot-value this name:))
    `(button
      (cues ,@(map ->ccproj-sxml (slot-value this cues:)))
      (name ,(->string (slot-value this name:)))
      (x ,x)
      (y ,y)
      (width ,width)
      (height ,height)
      (brightness ,(slot-value this brightness:))
      (repeat ,(bool->number (slot-value this repeat:)))
      (id ,(slot-value this id:))
      (color ,(let ((c (slot-value this color:)))
                (string-join
                 (map ->string (color->RGB709 c))
                 ", "))))))


;;
;; Load
;;

(define (load-ccproj path)
  (let ((ccproj-sxml
         (with-input-from-file path
           (lambda () (ssax:xml->sxml (current-input-port) '())))))
    (chromacove-project-from-sxml ccproj-sxml)))

(define (load-ccscm path)
  (eval '(import ccschemer))
  (parameterize ((current-document (chromacove-project src-path: path)))
    (load path)
    (current-document)))


;;
;; Write
;;

(define (write-ccproj document path)
  (parameterize ((current-document document))
    (let ((sxml (->ccproj-sxml document)))
      (with-output-to-file path
        (lambda ()
          (SRV:send-reply
           (pre-post-order* sxml universal-conversion-rules*))
          (newline)))))
  (log-status "Wrote " path))

)
