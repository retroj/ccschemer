;; This file is part of ccschemer.
;; Copyright (C) 2016  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(module ccschemer-color
    *

(import (chicken base)
        (chicken module)
        scheme)

(import (srfi 1)
        color)

(reexport color)


;; hsl
;;

(define (rgb->hsl r g b)
  (let* ((M (max r g b))
         (m (min r g b))
         (l (* 0.5 (+ M m))))
    (cond
     ((= M m)
      (list 0.0 0.0 l))
     (else
      (let* ((c (- M m))
             (s (/ c (- 1.0 (abs (- (* 2.0 l) 1.0)))))
             (h (cond
                 ((= r M)
                  (- (/ (- M b) c)
                     (/ (- M g) c)))
                 ((= g M)
                  (+ 2.0 (- (/ (- M r) c)
                            (/ (- M b) c))))
                 (else
                  (+ 4.0 (- (/ (- M g) c)
                            (/ (- M r) c)))))))
        (list (/ h 6.0) s l))))))


;; hsv
;;

(define (rgb->hsv r g b)
  (let* ((v (max r g b))
         (mn (min r g b)))
    (cond
     ((= v mn)
      (list 0.0 0.0 v))
     (else
      (let ((s (/ (- v mn) v))
            (h (cond
                ((= r v)
                 (- (/ (- v b) (- v mn))
                    (/ (- v g) (- v mn))))
                ((= g v)
                 (+ 2.0 (- (/ (- v r) (- v mn))
                           (/ (- v b) (- v mn)))))
                (else
                 (+ 4.0 (- (/ (- v g) (- v mn))
                           (/ (- v r) (- v mn))))))))
        (list (/ h 6.0) s v))))))


(define (hsv->rgb h s v)
  (cond
   ((<= s 0.0) (list v v v))
   (else
    (let* ((hh (* 6.0 (- h (truncate h))))
           (i (truncate hh))
           (f (- hh i))
           (p (* v (- 1.0 s)))
           (q (* v (- 1.0 (* s f))))
           (t (* v (- 1.0 (* s (- 1.0 f))))))
      (cond
       ((= i 0) (list v t p))
       ((= i 1) (list q v p))
       ((= i 2) (list p v t))
       ((= i 3) (list p q v))
       ((= i 4) (list t p v))
       ((= i 5) (list v p q)))))))



;;
;;

(define rgb color:RGB709)

(define (hsv h s v)
  (apply color:RGB709 (hsv->rgb h s v)))

)
