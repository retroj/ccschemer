;; This file is part of ccschemer.
;; Copyright (C) 2014-2015  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(module ccschemer-language
    *

  (import (chicken base)
          (chicken condition)
          (chicken module)
          (chicken random)
          scheme)

(import matchable)

(reexport matchable)

(define random pseudo-random-integer)

(define (ccschemer-error loc msg . args)
  (signal
   (make-composite-condition
    (make-property-condition 'ccschemer)
    (make-property-condition 'exn
                             'location loc
                             'message msg
                             'arguments args))))

(define L list)

(define rest cdr)

(define (first/default lst default)
  (if (pair? lst)
      (car lst)
      default))

(define (make-counter start)
  (let ((c (make-parameter start)))
    (lambda ()
      (let ((n (c)))
        (c (+ 1 n))
        n))))

(define (random/float #!optional (min 0.0) (max 1.0))
  (+ min (* (exact->inexact (random 32768))
            (/ (- max min) 32768))))

(define (random/integer min max)
  (inexact->exact (+ min (random (- max min -1)))))

(define (random-integer-generator min max #!key norepeat)
  (let ((previous #f)
        (randmax (+ 1 (- max min))))
    (define (generator)
      (let ((n (+ min (random randmax))))
        (cond
         ((and norepeat (eqv? n previous))
          (generator))
         (else
          (set! previous n)
          n))))
    generator))

(define (random-list-element-generator lst #!key norepeat)
  (let ((nelem (length lst))
        (previous #f))
    (define (generator)
      (let ((n (random nelem)))
        (cond
         ((and norepeat (eqv? n previous))
          (generator))
         (else
          (set! previous n)
          (list-ref lst n)))))
    generator))

(define-syntax bind
  (syntax-rules ()
    ((bind pattern exp . body)
     (match exp (pattern . body)))))

(define-syntax bind-lambda
  (syntax-rules ()
    ((bind-lambda pattern . body)
     (match-lambda (pattern . body)))))

(define-syntax bind-lambda*
  (syntax-rules ()
    ((bind-lambda* pattern . body)
     (match-lambda* (pattern . body)))))

)
