;; This file is part of ccschemer.
;; Copyright (C) 2014-2015  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(module ccschemer-event
    *

(import (chicken base)
        scheme)

(import (srfi 1)
        (only list-utils plist->alist)
        matchable
        regex)

(import ccschemer-language)

(define (hms->seconds hms)
  (and-let*
      ((match (string-match '(: (?? ($ (+ numeric)) #\:)
                                (? ($ (+ numeric)) #\:)
                                ($ (+ numeric) (? #\. (+ numeric))))
                            hms))
       (h (string->number (or (list-ref match 1) "0")))
       (m (string->number (or (list-ref match 2) "0")))
       (s (string->number (list-ref match 3))))
    (+ (* 3600 h) (* 60 m) s)))

(define (event-timestamp->seconds t)
  (if (symbol? t)
      (hms->seconds (symbol->string t))
      t))

(define make-event
  (match-lambda
   ((timestamp (attack sustain decay) . keywords)
    (cons* (event-timestamp->seconds timestamp)
           `(,(event-timestamp->seconds attack)
             ,(event-timestamp->seconds sustain)
             ,(event-timestamp->seconds decay))
           (plist->alist keywords)))
   ((timestamp end-time . keywords)
    (make-event
     (cons* timestamp
            ;; convert end-time to (relative) decay
            `(#f #f ,(and-let* ((end-time-sec (event-timestamp->seconds end-time)))
                       (- end-time-sec (event-timestamp->seconds timestamp))))
            keywords)))
   (timestamp
    (make-event
     (cons* timestamp '(#f #f #f) '())))))

(define (make-event-list spec #!key (tempo 60.0) (default-envelope #f) (properties '()))
  (let ((tempo (/ 1.0 (/ tempo 60.0))))
    (map-in-order
     (lambda (e-spec)
       (let* ((event (make-event e-spec))
              (envelope (if (procedure? default-envelope)
                            (default-envelope event)
                            default-envelope))
              (event (event-scale-timestamp
                      (if envelope
                          (apply event-apply-default-envelope event envelope)
                          event)
                      tempo)))
         (apply event-append-properties event
                (if (procedure? properties)
                    (properties event)
                    properties))))
     spec)))

(define (event-scale-timestamp event factor)
  (bind (timestamp envelope . keywords)
        event
        (cons* (* factor timestamp) envelope keywords)))

(define (event-apply-default-envelope event default-attack default-sustain default-decay)
  (bind (timestamp (attack sustain decay) . keywords)
        event
        (cons* timestamp
               (list (or attack default-attack)
                     (or sustain default-sustain)
                     (or decay default-decay))
               keywords)))

(define (event-add-properties event . plist)
  (bind (timestamp envelope . keywords)
        event
        (cons* timestamp envelope
               (append (plist->alist plist) keywords))))

(define (event-append-properties event . plist)
  (bind (timestamp envelope . keywords)
        event
        (cons* timestamp envelope
               (append keywords (plist->alist plist)))))

(define event-timestamp first)

(define event-envelope second)

(define event-properties cddr)

(define (event-property event keyword)
  (alist-ref keyword (event-properties event)))

)
