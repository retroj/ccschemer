#!/bin/sh
#| -*- scheme -*-
exec csi -s "$0" "$@"
|#

;; This file is part of ccschemer.
;; Copyright (C) 2014-2015  John J. Foerch
;;
;; ccschemer is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; ccschemer is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with ccschemer.  If not, see <http://www.gnu.org/licenses/>.

(import (chicken base)
        (chicken process-context)
        (chicken string)
        scheme)

(import (srfi 1)
        (srfi 13)
        comparse
        fmt
        matchable
        regex)

(include-relative "ccschemer.scm")
(import ccschemer)

(define (usage)
  (fmt #f "usage: ccschemer <document-clause>... [-o output-filename]" nl
       nl
       "  document-clause     <document> [selector/operator]..." nl
       "  document            a .ccscm or a .ccproj file" nl
       "  output-filename     a .ccproj filename for output" nl
       nl
       "selectors:" nl
       "  -b NAME             select button by full name" nl
       "  -bs PATTERN         select buttons by substring pattern" nl
       "  -c NAME             select cue by full name" nl
       "  -cs PATTERN         select cues by substring pattern" nl
       nl
       "operators:" nl
       "  -delete" nl
       "  -print" nl
       nl
       "The ccschemer command line works from left to right.  Load one or more" nl
       "documents.  Each may be followed by any number of selectors and operators" nl
       "to alter that document.  After all documents have been loaded, the selections" nl
       "from all documents (or all of their contents where no selection was specfied)" nl
       "will be combined to create the output document.  If an output filename has" nl
       "not been specified, one will be derived from the first document by changing" nl
       "the extension to .ccproj."))

(define documents (make-parameter (list)))

(define (document-spec? s)
  (and (> (string-length s) 0)
       (not (string-prefix? "-" s))))


(define (execute-operations doc ops)
  ;; perform operations on doc, return doc
  (for-each (lambda (op) (op doc)) ops)
  doc)

(define (button-name-selector =)
  (lambda (pattern)
    (lambda (doc)
      (set! (slot-value doc selection:)
        (append
         (filter-map
          (lambda (button)
            (and (= (->string (slot-value button name:)) pattern)
                 button))
          (slot-value doc buttons:))
         (or (slot-value doc selection:) '()))))))

(define (cue-name-selector =)
  (lambda (pattern)
    (lambda (doc)
      (set! (slot-value doc selection:)
        (append
         (filter-map
          (lambda (cue)
            (and (= (->string (slot-value cue name:)) pattern)
                 cue))
          (slot-value doc cues:))
         (or (slot-value doc selection:) '()))))))

(define selectors
  `((b . ,(button-name-selector string=?))
    (bs . ,(button-name-selector string-contains))
    (c . ,(cue-name-selector string=?))
    (cs . ,(cue-name-selector string-contains))))

(define selectors-regexp
  `(: #\- (? #\-) ($ (or ,@(map (o ->string car) selectors)))))

(define operators
  `((delete . ,(lambda (doc)
                 (match-let (((cues buttons)
                              ;;XXX: we should do something more nuanced
                              ;;     than include-button-cues.  if a cue
                              ;;     is used in multiple buttons, we
                              ;;     should only warn about deleting it if
                              ;;     its deletion was explicitly
                              ;;     requested.
                              (chromacove-project-selection doc include-button-cues: #t reset: #t)))
                   (set! (slot-value doc buttons:)
                     (lset-difference eq? (slot-value doc buttons:) buttons))

                   (let* ((cuenames (map (lambda (cue) (slot-value cue name:)) cues))
                          (usedcues (append-map
                                     (lambda (button)
                                       (map (lambda (ref) (slot-value ref cue:))
                                            (slot-value button cues:)))
                                     (slot-value doc buttons:)))
                          (cantdelete (lset-intersection eq? usedcues cuenames)))

                     ;;XXX: what should we do about cues that still have references in buttons?
                     (set! (slot-value doc cues:)
                       (lset-difference eq? (slot-value doc cues:) cues))))))
    (print . ,(lambda (doc)
                (match-let (((cues buttons)
                             (chromacove-project-selection doc include-button-cues: #t)))
                  (fmt #t (map (lambda (cue) (slot-value cue name:)) cues) nl
                       buttons nl nl))))))

(define operators-regexp
  `(: #\- (? #\-) ($ (or ,@(map (o ->string car) operators)))))


;; Parsers
;;

(define document-spec
  (satisfies document-spec?))

(define selector-clause
  (bind
   (sequence
     (satisfies (lambda (s) (string-match selectors-regexp s)))
     item)
   (match-lambda
     ((op arg)
      (let* ((opsym (string->symbol (second (string-match selectors-regexp op))))
             (action (alist-ref opsym selectors)))
        (result (action arg)))))))

(define operator
  (bind
   (satisfies (lambda (s) (string-match operators-regexp s)))
   (lambda (op)
     (let* ((opsym (string->symbol (second (string-match operators-regexp op))))
            (action (alist-ref opsym operators)))
       (result action)))))

(define document-clause
  (bind
   (sequence
     document-spec
     (zero-or-more (any-of selector-clause operator)))
   (match-lambda
     ((path selectors-and-operators)
      (result (execute-operations
               (load-ccscm path)
               selectors-and-operators))))))

(define output-clause
  (bind (sequence (satisfies (lambda (x) (string=? "-o" x))) item)
        (o result second)))

(define command-line
  (followed-by
   (sequence
     (one-or-more document-clause)
     (maybe output-clause))
   end-of-input))


;; Main
;;

(define (execute-command-line documents output)
  (when (null? documents)
    (program-abort "no documents"))
  (let ((output (or output
                    (chromacove-project-generate-output-name
                     (first documents))
                    "out.ccproj")))
    ;; apply selection to all documents
    (for-each
     (lambda (doc)
       (when (slot-value doc selection:)
         (match-let (((keep-cues keep-buttons)
                      (chromacove-project-selection
                       doc include-button-cues: #t reset: #t)))
           (set! (slot-value doc buttons:)
             (filter (lambda (button) (member button keep-buttons))
                     (slot-value doc buttons:)))
           (set! (slot-value doc cues:)
             (filter (lambda (cue) (member cue keep-cues))
                     (slot-value doc cues:))))))
     documents)
    ;; merge all into first document
    ;;XXX: also need to import all cues
    (parameterize ((current-document (first documents)))
      (for-each
       import-buttons
       (cdr documents)))
    (write-ccproj (first documents) output)))

(call-with-values
    (lambda _ (parse command-line (command-line-arguments)))
  (match-lambda*
    (((documents output) remainder)
     (execute-command-line documents output))
    ((#f remainder)
     (program-abort
      (fmt #f "failed to parse command line" nl "    "
           (parser-input->list remainder) nl nl
           (usage))))))
