
(include-relative "../src/ccschemer.scm")

(import ccschemer)

(import (srfi 1))
(import miscmacros)
(import test)

(define pi tau/2)

(define list-approx=?
  (let ((approx=? (current-test-comparator)))
    (lambda (ls1 ls2)
      (and (= (length ls1) (length ls2))
           (every approx=? ls1 ls2)))))

(define list-approx=/with-predicates?
  (let* ((cmp (current-test-comparator))
         (approx=?
          (lambda (a b)
            (if (procedure? a)
                (a b)
                (cmp a b)))))
    (lambda (ls1 ls2)
      (and (= (length ls1) (length ls2))
           (every approx=? ls1 ls2)))))

(define (cue-sequence->flat-list seq)
  (define (cueref->plist ref)
    (list (slot-value ref cue:)
          (exact->inexact (slot-value ref fade:))
          (exact->inexact (slot-value ref hold:))))
  (flatten (map cueref->plist seq)))

(test-begin "ccschemer")

(test-group
 "angles"
 (parameterize ((angle-scale 360)
                (angle-offset 0))
   (test "angle-scale-1" (/ tau 4) (angle->radians 90))
   (test "speed-1" 1.0 (translate-speed 360))))

(test-group
 "normalize-radians"
 (test "normalize-radians-1" 0.0 (normalize-radians tau))
 (test "normalize-radians-2" pi (normalize-radians pi))
 (test "normalize-radians-3" pi (normalize-radians (- pi)))
 (test "normalize-radians-4" 1.0 (normalize-radians (+ tau 1)))
 (test "normalize-radians-5" (- tau 1.0) (normalize-radians -1)))

(test-group
 "interpolate-angles"
 (test '() (interpolate-angles 0 1 0))
 (test '(1/2) (interpolate-angles 0 1 1))
 (test '(1/3 2/3) (interpolate-angles 0 1 2))
 (test '(1) (interpolate-angles 1 1 1)))

(test-group
 "interpolate-angles-list"
 (test '() (interpolate-angles-list '()))
 (test '(1) (interpolate-angles-list '(1)))
 (test '(1 2) (interpolate-angles-list '(1 2)))
 (test '(1 2 3) (interpolate-angles-list '(1 2 3)))
 (test '(1 2 3) (interpolate-angles-list '(1 #f 3)))
 (test '(1 4/3 5/3 2) (interpolate-angles-list '(1 #f #f 2)))
 (test '(1 1) (interpolate-angles-list '(1 #f)))
 (test '(1 1 1) (interpolate-angles-list '(#f 1 #f)))
 (test '(1 1) (interpolate-angles-list '(#f 1)))
 (test '(0) (interpolate-angles-list '(#f)))
 (test '(0 1/2) (interpolate-angles-list '(#f #f)))
 (test '(0 1/3 2/3) (interpolate-angles-list '(#f #f #f)))
 (test '(1 1 1) (interpolate-angles-list '(1 #f 1)))
 (test '(0 1/3 2/3) (parameterize ((angle-scale -1))
                      (interpolate-angles-list '(#f #f #f)))))

(test-group
 "buttons"
 (let ((def-wid (button-width))
       (def-hei (button-height)))
   (parameterize ((button-width (+ 1 def-wid))
                  (button-height (+ 1 def-hei)))
     (test (+ 1 def-wid) (rect-width (slot-value (make <button>) rect:)))
     (test (+ 1 def-hei) (rect-height (slot-value (make <button>) rect:))))))

(define (compute-layer-start-and-stop-angles #!optional start stop
                                             (scale 1.0) (offset 0.0))
  (parameterize ((current-document (chromacove-project))
                 (angle-scale scale)
                 (angle-offset offset))
    (let ((attrs (layer-common-ccproj-sxml
                  (apply make <layer>
                         (append (if start (list startangle: start) '())
                                 (if stop (list stopangle: stop) '()))))))
      (list (cadr (assq 'startAngle attrs))
            (cadr (assq 'stopAngle attrs))))))

(test-group
 "layer startAngle, stopAngle"
 (test `(0.0 ,tau) (compute-layer-start-and-stop-angles))
 (test `(0.0 ,tau) (compute-layer-start-and-stop-angles #f #f 1.0 0.5)))

(define (gradient-node-order-helper)
  (parameterize ((current-document (chromacove-project)))
    (map
     (lambda (x) (cadr (assq 'angle (cdr x))))
     (cdr
      (assq 'nodes
            (cdr
             (assq 'gradient
                   (cdr
                    (->ccproj-sxml
                     (gradient
                      (list-tabulate
                       2 (lambda (i) (node (hsv (/ i 2) 1 1))))))))))))))

(test-group
 "gradient node order"
 (test #t (let ((angles (gradient-node-order-helper)))
            (< (car angles) (cadr angles))))
 (test #t (parameterize ((angle-scale -1))
            (let ((angles (gradient-node-order-helper)))
              (< (car angles) (cadr angles))))))

(test-group
 "layout place-widget"
 (parameterize ((page-width 800))
   (parameterize ((current-layout (make <rows-layout>)))
     (dotimes (i 8) (place-widget 100 100))
     (test 0 (slot-value (current-layout) pt-y:))
     (place-widget 100 100)
     (test 100 (slot-value (current-layout) pt-x:))
     (test 100 (slot-value (current-layout) pt-y:)))

   (parameterize ((current-layout (make <columns-layout>)))
     (place-widget 100 100)
     (test 0 (slot-value (current-layout) pt-x:))
     (test 100 (slot-value (current-layout) pt-y:))
     (line-break)
     (test 100 (slot-value (current-layout) pt-x:))
     (test 0 (slot-value (current-layout) pt-y:)))

   (parameterize ((current-layout (make <rows-layout>)))
     (place-widget 50 50)
     (layout-append! (make <columns-layout>) skip: 10)
     (test 60 (slot-value (current-layout) top:))
     (test 60 (slot-value (current-layout) pt-y:)))))

(test-group
 "cues"
 (test-error "duplicate cue names produce error"
             (parameterize ((current-document (chromacove-project)))
               (cue 'red)
               (cue 'red)))
 (test-error "get-cue with 2 args raises error when cue not found"
             (get-cue (chromacove-project) 'non-existing-cue))
 (test "get-cue with 3 args returns default when cue not found"
       'hello
       (get-cue (chromacove-project) 'non-existing-cue 'hello)))

(test-group
 "sxml translation"
 (define (cueref-sxml? x)
   (and (pair? x)
        (eq? 'cue (car x))
        (and-let* ((r (alist-ref 'cue (cdr x))))
          (number? (car r)))))

 (test-assert "cue has default group"
              (parameterize ((current-document (chromacove-project)))
                (assoc 'group (cdr (->ccproj-sxml (cue 'test-cue))))))

 (parameterize ((current-document (chromacove-project)))
   (let ((b (button 'a cues: (cue 'a))))
     (test-assert "button wraps bare cue in cueref"
                  (every cueref-sxml? (alist-ref 'cues (cdr (->ccproj-sxml b))))))))

(test-group
 "events"
 (test "event-properties returns an alist"
       '((foo: . 3))
       (event-properties (make-event '(1 #f foo: 3))))
 (test "event-property"
       3
       (event-property (make-event '(1 #f foo: 3)) foo:)))

(test-group
 "make-cue-sequence"
 (define (test-project)
   (parameterize ((current-document (chromacove-project)))
     (cue 'black (gradient (L (node color:black))))
     (cue 'red (gradient (L (node (rgb 1 0 0)))))
     (cue 'green (gradient (L (node (rgb 0 1 0)))))
     (cue 'blue (gradient (L (node (rgb 0 0 1)))))
     (current-document)))

 (parameterize ((current-document (test-project)))
   (test-assert "sustain and delay are #f, means no fade"
                (list= equal?
                       '(red 0.0 1.0
                         green 0.0 1.0
                         blue 0.0 0.0)
                       (cue-sequence->flat-list
                        (make-cue-sequence
                         (make-event-list '((0 #f cue: red)
                                            (1 #f cue: green)
                                            (2 #f cue: blue))))))))

 (parameterize ((current-document (test-project)))
   (test-assert "cues-add 01" (cue? (cues-add '()
                                              (get-cue (current-document) 'red)
                                              (get-cue (current-document) 'green)))))

 (parameterize ((current-test-comparator list-approx=/with-predicates?))
   (parameterize ((current-document (test-project)))
     (test "envelope 01 (non-overlapping events)"
           '(red 0.0 0.1
             black 0.0 0.9
             green 0.0 0.1
             black 0.0 0.9
             blue 0.0 0.1
             black 0.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 0.0 0.1 0.0))
              (make-event-list '((0 #f cue: red)
                                 (1 #f cue: green)
                                 (2 #f cue: blue))))))))

  (parameterize ((current-document (test-project)))
     (test "envelope !01 (non-overlapping events)"
           '(red 0.0 0.1
             black 0.0 0.9
             green 0.0 0.1
             black 0.0 0.9
             blue 0.0 0.1
             black 0.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 0.0 0.1 0.0))
              (make-event-list '((0 #f cue: red)
                                 (1 #f cue: green)
                                 (2 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test "envelope 02 (non-overlapping events)"
           '(red 1.0 1.0
             black 1.0 1.0
             green 1.0 1.0
             black 1.0 1.0
             blue 1.0 1.0
             black 1.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 1 1 1))
              (make-event-list '((1 #f cue: red)
                                 (5 #f cue: green)
                                 (9 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test "envelope 03 (mix-mode = simple, overlapping sustain)"
           '(red 1.0 0.5
             green 0.0 0.5
             blue 0.0 1.0
             black 1.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 1 1 1))
              (make-event-list '((1.0 #f cue: red)
                                 (1.5 #f cue: green)
                                 (2.0 #f cue: blue))))
             mix-mode: 'simple))))

   (parameterize ((current-document (test-project)))
     (test "envelope 04 (simplified mixing)"
           `(red 1.0 1.0
             ,symbol? 1.0 0.0
             green 0.5 1.0
             ,symbol? 1.0 0.0
             blue 0.5 1.0
             black 2.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 1 1 2))
              (make-event-list '((1.0 #f cue: red)
                                 (3.5 #f cue: green)
                                 (6.0 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test "envelope 05 (simplified mixing, decay overlap attack)"
           `(red 2.0 1.0
             ,symbol? 1.5 0.0
             green 1.5 1.0
             ,symbol? 1.5 0.0
             blue 1.5 1.0
             black 2.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 2 1 2))
              (make-event-list '((2.0 #f cue: red)
                                 (6.0 #f cue: green)
                                 (10.0 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test "envelope 06 (simplified mixing, zero decay)"
           `(red 1.0 1.0
             ,symbol? 0.0 0.0
             green 0.5 1.0
             ,symbol? 0.0 0.0
             blue 0.5 1.0
             black 0.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 1 1 0))
              (make-event-list '((1.0 #f cue: red)
                                 (2.5 #f cue: green)
                                 (4.0 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test "envelope 07 (simplified mixing, zero attack)"
           `(red 0.0 1.0
             ,symbol? 0.5 0.0
             green 0.0 1.0
             ,symbol? 0.5 0.0
             blue 0.0 1.0
             black 1.0 0.0)
           (cue-sequence->flat-list
            (make-cue-sequence
             (map
              (lambda (event)
                (event-apply-default-envelope event 0 1 1))
              (make-event-list '((1.0 #f cue: red)
                                 (2.5 #f cue: green)
                                 (4.0 #f cue: blue))))))))

   (parameterize ((current-document (test-project)))
     (test-error "invalid mix-mode produces error"
                 (make-cue-sequence
                  (make-event-list '((0.0 (1 1 1) cue: red)
                                     (1.5 (1 1 1) cue: green)))
                  mix-mode: 'invalid)))))

(test-end "ccschemer")

(test-exit)
