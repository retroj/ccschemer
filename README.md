
# Overview

Ccschemer is a tool for generating and manipulating CCDesigner ccproj
files.  It does so by providing a programming API by which the ccproj
document can be defined with Scheme code.  You write this scheme code in a
.ccscm file, then run ccschemer on it, and ccschemer produces the
corresponding ccproj file.

Cues and buttons can be defined within the .ccscm file, as well as
imported from other ccproj files.  The arguments to the forms that define
cues and buttons have a mostly one-to-one relationship with their
representation in the ccproj file.  Here is a simple example:

    (chromacove-version "1.26 beta")
    (cue 'a-cue (solid-color (rgb 1 0 0)))
    (button 'a cues: (cueref 'a-cue) brightness: 0.25)

# Pros and Cons

A brief confession: ccschemer is a hack.  You probably shouldn't use it.
It came about as an experiment and an expedient to get a job done that
CCDesigner couldn't do when it wasn't an option to replace CCDesigner with
other software.  While ccschemer may do what you need, it should be viewed
as a workaround, not a final solution.  Here are some points to consider
before jumping in.

## Why you SHOULDN'T use ccschemer:

 - It's not user-friendly.
 - It's not robustly tested.
 - You will need to learn the basics of the CHICKEN Scheme programming
   language to use it.
 - Some things are harder in ccschemer than they are in CCDesigner.
 - The API and command-line tool are a work in progress; what you write
   now will probably need to be maintained later.

## Why you SHOULD use ccschemer:

 - Some things are easy in ccschemer that are hard, impractical, or
   impossible in CCDesigner.
 - Ccschemer can merge, prune, and do other maintenance tasks on ccproj
   files.
 - Ccschemer files are plain text.  You use your favorite text editor to
   write them and you get features like undo and version control for free.
 - The entire scheme programming language is available to help you create
   cues and sequences.  This means flexibility and portability.
 - Free as in Freedom Open Source software.  If it doesn't do what you
   want, you have the source.
 - Programming is fun!

# Installation

## From Source

### CHICKEN Scheme

Ccschemer is a CHICKEN Scheme program.  CHICKEN Scheme is an
implementation of the Scheme programming language that runs on all major
OSes, has a large collection of extensions, and provides both an
interpreter and a compiler.

 - CHICKEN Scheme website: http://call-cc.org/
 - Installation instructions for various OSes:
   http://wiki.call-cc.org/platforms
 - CHICKEN installer for Windows, that I have used successfully:
   https://bitbucket.org/roti/chicken-installer/downloads

I ran into one difficulty with CHICKEN on Windows.  In order for the
`chicken-install` program to work, I had to manually change the
permissions on the CHICKEN installation directory to allow unprivileged
writing.

#### ccschemer

 - Project host: https://bitbucket.org/retroj/ccschemer

Obtain ccschemer.  You can use git to clone the project, or download a
snapshot.

 - Snapshot download: https://bitbucket.org/retroj/ccschemer
 - Git clone url: git@bitbucket.org:retroj/ccschemer.git
 - Git clone url: https://bitbucket.org/retroj/ccschemer.git

In a command shell, enter the ccschemer project directory and run:

    chicken-install

On platforms other than Windows, you may need root privileges to install.
If you have sudo set up, that can be done as follows:

    chicken-install -s

# Usage

Ccschemer is a command line program that takes ccscm and ccproj files as
input, and produces a ccproj file as output.  For basic usage, call
ccschemer with one more more ccscm and ccproj files as arguments.  The
contents of all inputs will be combined into the output.

    ccschemer mylights.ccscm

The output filename will be derived from the first input filename, by
changing the extension to `.ccproj`.  To specify a different output
filename, use the `-o` option at the end of the command line:

    ccschemer mylights.ccscm -o out.ccproj

Some operations can also be done right on the command line with selectors
and operators.  Selectors and operators follow an input filename.
Selectors make a selection, so only the specified buttons and cues will be
included in the output.  In this example, we will extract only the buttons
with "cool-sweep" in their name.

    ccschemer mylights.ccscm -bs cool-sweep

Operators perform some action on the selection.  In some cases, operators
also clear the selection, if it makes sense to do so.  To delete buttons
with "cool-sweep" in their name, make a selection like before, then use
the `-delete` operator.

    ccschemer mylights.ccscm -bs cool-sweep -delete

## Selectors

 - `-b NAME`: select a button by its full name
 - `-bs PATTERN`: select buttons by substring pattern
 - `-c NAME`: select a cue by its full name
 - `-cs PATTERN`: select cues by substring pattern

## Operators

 - `-delete`: delete selected cues and buttons; clears selection.
 - `-print`: print information about the selected cues and buttons.

# API

## Angles

Angles in ccschemer are normalized to a range of 0.0 to 1.0 by default,
and translated to radians in the output.  This allows you to input angles
as simple fractions.  The angle units and system can be changed for a
ccscm file by changing the values of the parameters `angle-scale` and
`angle-offset`.  The equation to translate a user-angle to a fractional
angle is:

    (/ (+ (angle-offset) angle)
       (angle-scale))

Angle-offset is first added to the supplied angle.  This allows the
orientation of the cove ring to be rotated.  Then the result of that is
divided by `angle-scale`.  This allows you to change the angular units or
flip the circe.  For example, to work in degrees and rotate the
light-circle by 180 degrees:

    (angle-scale 360)
    (angle-offset 180)

To swap clockwise with anticlockwise as well, just negate angle-scale:

    (angle-scale -360)
    (angle-offset 180)

## Colors

Ccschemer uses the color library from slib to represent colors.  The
constructors `rgb` and `hsv` are provided to make colors.

Alpha channels are represented with a separate slot (in <node>) from color
because alpha is not a color component, but a parameter to color
combination.

The following colors are predefined:

 - color:black
 - color:white

## Class Reference

Where `. properties` is indicated in a call form, that means a sequence
like this:

    key1: val1 key2: val2 ...

### <chromacove-project>

    (chromacove-project . properties)
    (load-ccproj path)

Data structure that represents a ccproj file.  One is automatically
created at runtime.  Buttons and cues defined or imported in the ccscm go
into it, and it gets written to disk as a ccproj after ccscm load is
finished.  See also `(current-document)`.

 - version: target CCDesigner version

### <node>

A node in a gradient layer.

    (node color . properties)

 - color: a color value, like (hsv 0.6 1 1)
 - color2: the color to use for r2 nodes; defaults to black.
 - angle: an angle; if omitted, value is interpolated from surrounding nodes.
 - r1: inner radius; when non-zero, the node represents two nodes of the
   same color with 2*r1 distance between them.
 - r2: outer radius; when non-zero, nodes of color2 are placed at this
   distance from the center of this node on either side, making a color spot.
 - bias:
 - alpha: 0 fully transparent; 1 fully opaque.

### <layer>

Layer is an abstract base class, not directly used.  Groups and gradients
are the two main types of layers.  All subclasses of <layer> have the
following common properties:

 - name:
 - startangle:
 - stopangle:
 - speed: rotation speed. Note, value is inverted from how it appears in
   CCDesigner.  A higher value means faster.
 - type: one of 'repeat, 'stop, 'ping-poing
 - tween: one of 'linear, 'quadratic-ease-in, 'quadratic-ease-out,
   'quadratic-ease-in-out, 'cubic-ease-in, 'cubic-ease-out,
   'cubic-ease-in-out, 'quartic-ease-in, 'quartic-ease-out,
   'quartic-ease-in-out, 'sine-ease-in, 'sine-ease-out, 'sine-ease-in-out,
   'exponential-ease-in, 'exponential-ease-out, 'exponential-ease-in-out,
   'circular-ease-in, 'circular-ease-out, 'circular-ease-in-out
 - blend: one of 'normal, 'add, 'subtract, 'multiply, 'tint, 'linear-burn,
   'saturate, 'overlay
 - visible:
 - antialiased:

#### <group>

    (group layers . properties)

 - layers: layers and groups within this group.  may be a single item or a
   list of them.

#### <gradient>

    (gradient nodes . properties)

 - nodes: list of nodes

##### <solid-color>

A gradient of one color.

    (solid-color color . properties)

 - color:

##### <color-spot>

A black (or color2) gradient with a spot of some color.

    (color-spot color angle . properties)

 - color:
 - color2: outer color, as for <node>; defaults to black.
 - angle:
 - r1: inner radius, as for <node>
 - r2: outer radius, as for <node>

### <cue>

A single state of the light ring and/or auxiliary dmx universe.

    (cue name . layers+properties)

 - layers+properties: actually just properties, but the symbol `layers:`
   will be prepended if the first item is not a property name.  It just
   allows for a convenient shorthand.

 - layers: layers and groups within this cue.  may be a single item or a
   list of them.  a cue has exactly one top-level group containing all
   groups and layers.  if a single group is given here, it will be that
   top-level group, otherwise the top-level group will be created
   automatically.

### <cueref>

A reference to a cue with an associated fade time and hold time, in a
button.

    (cueref cue . properties)

 - fade: time to fade up to this cue (seconds).
 - hold: time to stay on this cue before fading to the next in the
   sequence (seconds).

### <button>

    (button name . properties)

 - name
 - cues: list of cuerefs
 - rect: rectangle giving placement and size of the button on the page.
   Layouts are normally used to generate this.
 - brightness: brightness multiple between 0 and 1 for this light
   sequence.
 - repeat: boolean - when #t, the sequence loops.
 - id: external identifier for calling this button via RUN_CUE.  Max 7
   digits.
 - color: background color of the button.

# Defining Cues

# Defining Buttons

# Miscellaneous

## chromacove-version

A <chromacove-project> has a `version` slot which can be used to identify
the target version of CCDesigner.  Some small differences have been found
in ccproj format from version to version, so ccschemer uses this
information to format the output accordingly.

For convenience, the procedure `chromacove-version` is provided to get and
set the version of the `(current-document)`.  A ccscm script should
typically use this to set the desired target version:

    (chromacove-version "1.26 beta")
